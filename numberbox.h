#ifndef NUMBERBOX
#define NUMBERBOX

#include "iqbox.h"

class NumberBox : public iQBox
{
public:
    NumberBox() :
        iQBox{}
      , NodeRank{0}
      , BlockRank{0}
      , CurrentBlockRank{0}
      , Mode{ IncreasingMode::RL }
    {

    }

    ~NumberBox() = default;

    void setNodeRank(uint nodeRank) override
    {
        NodeRank = nodeRank;
    }

    void setBlockRank(uint blockRank) override
    {
        BlockRank = blockRank;
    }

    uint nodeRank() override
    {
        return NodeRank;
    }

    uint blockRank() override
    {
        return BlockRank;
    }

    uint currentBlockRank() override
    {
        return CurrentBlockRank;
    }

    void setIncreasingMode(IncreasingMode mode) override
    {
        Mode = mode;
    }

    IncreasingMode mode() override
    {
        return Mode;
    }

protected:
    uint NodeRank;
    uint BlockRank;
    uint CurrentBlockRank;

    IncreasingMode Mode;
};

#endif // NUMBERBOX

