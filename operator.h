#ifndef OPERATOR_H
#define OPERATOR_H

#include "ioperator.h"
#include "iqbox.h"

class Operator : public iOperator
{
public:
    Operator(Block block, std::string name, iQBox* box) :
        Box{box}
      , CurrentBlock{block}
      , Name{name}
      , Matrix{ }
      , Storage{ }
    {

    }

    virtual ~Operator() = default;

    iOperator &operator = (const iOperator* Other) override
    {
        //check this is used??
        CurrentBlock = Other->block();
        Name = Other->name();
        Matrix = Other->mat();
        //Box = Other->Box; //TODO
        //Storage = Other->Storage(); //TODO check this
        return *this;
    }

    arma::cx_mat& mat() override
    {
        return Matrix;
    }

    arma::cx_mat mat() const override
    {
        return Matrix;
    }

    arma::uword dim() override
    {
        return sqrt( Matrix.size() );
    }

    virtual void recalculate() override
    {
       //empty
    }

    virtual void increaseLeft() override
    {
       //empty
    }

    virtual void increaseRight() override
    {
       //empty
    }

    virtual void decrease() override
    {
        //assert(decreseIsEnable() && "Decrese is not enable");
        Matrix = Storage.back();
        Storage.pop_back();
    }

    bool decreseIsEnable() override
    {
        return !Storage.empty();
    }

    void rebaseWithTruncation(const std::vector<arma::cx_vec>& eigenVectors, uint size) override
    {
        arma::cx_mat NewMatrix(size, size, arma::fill::zeros);

        for(uint i=0; i<size; i++){
            for(uint j=0; j<size; j++){
                NewMatrix(i,j) = as_scalar(eigenVectors[i].t() * Matrix * eigenVectors[j]);
            }
        }
        Matrix = NewMatrix;
    }

    Block block() const override
    {
        return CurrentBlock;
    }

    std::string name() const override
    {
        return Name;
    }

    void updateStorage() override
    {
        Storage.push_back(Matrix);
    }

    virtual void newCycle() override
    {
        Storage.clear();
    }

protected:
    arma::cx_mat oI(std::string name)
    {
        return Box->oI(name);
    }

    arma::cx_mat oII(std::string name)
    {
        return Box->oII(name);
    }

    complex pI(std::string name)
    {
        return Box->pI(name);
    }

    complex pII(std::string name)
    {
        return Box->pII(name);
    }

protected:
     iQBox* Box;
protected: //change to private
    Block CurrentBlock;
    std::string Name;
    arma::cx_mat Matrix;
    std::vector<arma::cx_mat> Storage;
};


#endif // OPERATOR_H
