#ifndef STATEBOX
#define STATEBOX

#include "repository.h"

class StateBox : public Repository
{
public:
    StateBox() :
        Repository{ }
      , DensityMatrixA{}
      , DensityMatrixB{}
    {

    }

    ~StateBox() = default;

    arma::cx_mat densityMatrix(Block block, uint stateNumber) override
    {
        assert("Wrong state number" && (stateNumber<=1)); //TODO
        if(block == Block::A){
            return DensityMatrixA;
        } else if(block == Block::B){
            return DensityMatrixB;
        } else if(block == Block::AB){
            return (DensityMatrixA ^ DensityMatrixB);
        }
        assert("Unknown block" && false);
        return arma::cx_mat{};
    }

protected:
    void calculateDensityMatrices(const std::vector<Eigen> &eigensA, const std::vector<Eigen> &eigensB)
    {
        calculateDensityMatrixA(eigensA);
        calculateDensityMatrixB(eigensB);
    }

    void clearDensityMatrices()
    {
        //todo when we will have matrices vector prepare this
    }

private:
    void calculateDensityMatrixA(const std::vector<Eigen> &eigensA)
    {
        DensityMatrixA.resize(NumberBox::BlockRank,NumberBox::BlockRank);
        DensityMatrixA.zeros();
        for(uint i=0; i<NumberBox::BlockRank; i++){
            DensityMatrixA(i,i) = eigensA[i].Value*eigensA[i].Value;
        }
        DensityMatrixA *= 1.0/trace(DensityMatrixA);
    }

    void calculateDensityMatrixB(const std::vector<Eigen> &eigensB)
    {
        DensityMatrixB.resize(NumberBox::BlockRank,NumberBox::BlockRank);
        DensityMatrixB.zeros();
        for(uint i=0; i<NumberBox::BlockRank; i++){
            DensityMatrixB(i,i) = eigensB[i].Value*eigensB[i].Value;
        }
        DensityMatrixB *= 1.0/trace(DensityMatrixB);
    }

protected:
    arma::cx_mat DensityMatrixA;
    arma::cx_mat DensityMatrixB;
};

#endif // STATEBOX

