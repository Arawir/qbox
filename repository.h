#ifndef REPOSITORY
#define REPOSITORY

#include "numberbox.h"
#include "operrep.h"

class Repository : public NumberBox
{
public:
    Repository() :
      NumberBox{ }
      , SuperblockHamiltonian{nullptr}
      , HamiltonianA{nullptr}
      , HamiltonianB{nullptr}
      , Observables{ }
      , Parameters{ }
    {

    }

    ~Repository()
    {
        delete SuperblockHamiltonian;
        delete HamiltonianA;
        delete HamiltonianB;
    }

    void setSuperblockHamiltonian(iOperator* hamiltonian) override
    {
        SuperblockHamiltonian = hamiltonian;
        Observables.push_back(hamiltonian);
    }

    void setHamiltonianA(iOperator* hamiltonian) override
    {
        HamiltonianA = hamiltonian;
        Observables.push_back(hamiltonian);
    }

    void setHamiltonianB(iOperator* hamiltonian) override
    {
        HamiltonianB = hamiltonian;
        Observables.push_back(hamiltonian);
    }


    void addObservable(iOperator* observable) override
    {
        Observables.push_back(observable);
    }

    void addParameter(iParameter* parameter) override
    {
        Parameters.push_back(parameter);
    }


    arma::cx_mat oI(std::string name) override
    {
        for(OperRep& observable : Observables){
            if(observable.Operator->name() == name){
                return observable.matrixI();
            }
        }
        assert("Unknown observable" && false);
        return nullptr;
    }

    arma::cx_mat oII(std::string name) override
    {
        for(OperRep& observable : Observables){
            if(observable.Operator->name() == name){
                return observable.matrixII();
            }
        }
        assert("Unknown observable" && false);
        return nullptr;
    }

    complex pI(std::string name) override //todo
    {
        for(auto& parameter : Parameters){
            if(parameter->name() == name){ return parameter->value(); }
        }
        assert("Unknown parameter" && false);
        return 0;
    }

    complex pII(std::string name) override //todo
    {
        for(auto& parameter : Parameters){
            if(parameter->name() == name){ return parameter->value(); }
        }
        assert("Unknown parameter" && false);
        return 0;
    }

    iOperator* observable(std::string name) override
    {
        for(OperRep& observable : Observables){
            if(observable.Operator->name() == name){ return observable.Operator; }
        }
        assert("Unknown observable" && false);
        return nullptr;
    }

    iParameter* parameter(std::string name) override
    {
        for(auto& parameter : Parameters){
            if(parameter->name() == name){ return parameter; }
        }
        assert("Unknown parameter" && false);
        return nullptr;
    }

protected:
    iOperator* SuperblockHamiltonian;
    iOperator* HamiltonianA;
    iOperator* HamiltonianB;
    std::vector<OperRep> Observables;
    std::vector<iParameter*> Parameters;
};

#endif // REPOSITORY

