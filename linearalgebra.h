#ifndef LINEARALGEBRA_H
#define LINEARALGEBRA_H

#include <vector>
#include "ioperator.h"
#include "basicphisics.h"

struct Eigen
{
    Eigen(double value, arma::cx_vec vector) :
        Value{value}
      , Vector{vector}
    {

    }
    double Value;
    arma::cx_vec Vector;
};

arma::cx_mat calculateDensityMatrix(const arma::cx_vec &BaseStateVector)
{
    return BaseStateVector*BaseStateVector.t();
}

arma::cx_mat partialTraceA(const arma::cx_mat& DMS, uint DMBSize)
{
    uint DMSSize = sqrt(DMS.size());
    arma::cx_mat DMB{ DMBSize, DMBSize, arma::fill::zeros };

    for(uint k=0; k<DMSSize; k+=DMBSize){
        for(uint i=0; i<DMBSize; i++){
            for(uint j=0; j<DMBSize; j++){
                DMB(i,j) += DMS(k+i,k+j);
            }
        }
    }

    return DMB;
}

arma::cx_mat partialTraceB(const arma::cx_mat& DMS, uint DMASize)
{
    uint DMSSize = sqrt(DMS.size());
    uint DMBSize = DMSSize/DMASize;
    arma::cx_mat DMA{ DMASize, DMASize, arma::fill::zeros};

    for(uint i=0; i<DMSSize; i+=DMBSize){
        for(uint j=0; j<DMSSize; j+=DMBSize){
            for(uint k=0; k<DMBSize; k++){
                DMA(i/DMBSize,j/DMBSize) += DMS(k+i,k+j);
            }
        }
    }

    return DMA;
}

std::vector<Eigen> calculateEigens(const arma::cx_mat& Matrix)
{
    arma::vec Eigenvalues;
    arma::cx_mat Eigenvectors;
    std::vector<Eigen> Eigens;

    arma::eig_sym(Eigenvalues, Eigenvectors, Matrix);

    for(uint i=0; i<Eigenvalues.size(); i++){
        Eigens.push_back( {Eigenvalues[i], Eigenvectors.col(i)});
    }

    return Eigens;
}

void sortEigensViaMagnitude(std::vector<Eigen> &eigens)
{
    for(uint i=0; i<eigens.size(); i++){
        for(uint j=0; j<eigens.size()-1; j++){
            double MagnitudeThis = eigens[j].Value * eigens[j].Value;
            double MagnitudeNext = eigens[j+1].Value * eigens[j+1].Value;
            if(MagnitudeThis<MagnitudeNext){
                std::swap(eigens[j], eigens[j+1]);
            }
        }
    }
}

void sortEigensViaValue(std::vector<Eigen> &eigens)
{
    for(uint i=0; i<eigens.size(); i++){
        for(uint j=0; j<eigens.size()-1; j++){
            double EigenvalueThis = eigens[j].Value;
            double EigenvalueNext = eigens[j+1].Value;
            if(EigenvalueNext<EigenvalueThis){
                std::swap(eigens[j], eigens[j+1]);
            }
        }
    }
}

std::vector<arma::cx_vec> extractEigenvectors(const std::vector<Eigen> &eigens)
{
    std::vector<arma::cx_vec> Eigenvectors;

    for(const Eigen& eigen : eigens){
        Eigenvectors.push_back( eigen.Vector );
    }

    return Eigenvectors;
}

std::vector<arma::cx_vec> mergeEigenvectors(const std::vector<arma::cx_vec> &vectorsA,
                                            const std::vector<arma::cx_vec> &vectorsB)
{
    std::vector<arma::cx_vec> EigenvectorsAB;

    for(const arma::cx_vec& vecA : vectorsA){
        for(const arma::cx_vec& vecB : vectorsB){
            EigenvectorsAB.push_back( kron(vecA,vecB) );
        }
    }

    return EigenvectorsAB;
}

#endif // LINEARALGEBRA_H
