#ifndef MATHBOX
#define MATHBOX

#include "statebox.h"

class MathBox : public StateBox
{
public:
    MathBox() :
        StateBox{ }
    {

    }

    ~MathBox() = default;

    double expectedValue(std::string observableName, uint stateNumber) override
    {
        arma::cx_mat ObservableMatrix = observable(observableName)->mat();
        Block ObservableBlock = observable(observableName)->block();
        arma::cx_mat DensityMatrix = StateBox::densityMatrix(ObservableBlock, stateNumber);

        return real( arma::as_scalar(trace(DensityMatrix * ObservableMatrix)) );
    }

protected:
    double expectedValue(iOperator* observable, uint stateNumber)
    {
        assert("Observable == nullptr" && observable != nullptr);
        arma::cx_mat ObservableMatrix = observable->mat();
        Block ObservableBlock = observable->block();
        arma::cx_mat DensityMatrix = StateBox::densityMatrix(ObservableBlock, stateNumber);

        return real( arma::as_scalar(trace(DensityMatrix * ObservableMatrix)) );
    }
};

#endif // MATHBOX

