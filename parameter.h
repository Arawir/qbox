#ifndef PARAMETER_H
#define PARAMETER_H

#include "iparameter.h"


class Parameter : public iParameter
{
public:
    Parameter(std::string name, complex value) :
        Name{name}
      , Value{value}
      , StartValue{value}
    {

    }

    ~Parameter() = default;

    complex& value() override
    {
        return Value;
    }

    complex value() const override
    {
        return Value;
    }

    std::string name() const override
    {
        return Name;
    }

    void reset() override
    {
        Value = StartValue;
    }

    iParameter* operator = (const complex Other) override
    {
        this->value() = Other;
        return this;
    }

private:
    std::string Name;
    complex Value;
    complex StartValue;
};

#endif // PARAMETER_H
