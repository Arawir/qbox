#ifndef IPARAMETER_H
#define IPARAMETER_H

#include <string>
#include <complex>

typedef std::complex<double> complex;

class iParameter
{
public:
    iParameter(){ }
    virtual ~iParameter() = default;

    virtual iParameter* operator = (const complex Other) = 0;
    virtual complex& value() = 0;
    virtual complex value() const = 0;
    virtual std::string name() const = 0;
    virtual void reset() = 0;
};

#endif // IPARAMETER_H
