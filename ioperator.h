#ifndef IOPERATOR_H
#define IOPERATOR_H

#include <armadillo>
#include "basicphisics.h"


enum class Block
{
    A, B, AB
};

class iOperator
{
public:
    iOperator(){ }
    virtual ~iOperator() = default;

    virtual iOperator &operator = (const iOperator* Other) = 0;

    virtual arma::cx_mat& mat() = 0;
    virtual arma::cx_mat mat() const = 0;
    virtual arma::uword dim() = 0;

    virtual bool decreseIsEnable() = 0;

    virtual void increaseLeft() = 0;
    virtual void increaseRight() = 0;
    virtual void recalculate() = 0;

    virtual void updateStorage() = 0;
    virtual void decrease() = 0;
    virtual void rebaseWithTruncation(const std::vector<arma::cx_vec>& eigenVectors, uint size) = 0;

    virtual Block block() const = 0;
    virtual std::string name() const = 0;

    virtual void newCycle() = 0;
};


#endif // IOPERATOR_H
