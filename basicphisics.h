#ifndef BASICPHISICS
#define BASICPHISICS

#include <armadillo>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <complex>

#include "lanczos.h"

#define im complex{0.0,1.0}
#define output std::cout << std::fixed << std::setprecision(4)

typedef std::complex<double> complex;

arma::cx_mat I(int dimension)
{
    if(dimension<1){ dimension = 1; }
    return arma::cx_mat{ static_cast<arma::uword>(dimension),
                      static_cast<arma::uword>(dimension),
                      arma::fill::eye};
}

arma::cx_vec calculateBaseState(const arma::cx_mat &Hamiltonian)
{
    auto Data = calculateBaseStateLanczos(Hamiltonian);
    return std::get<1>(Data);
}

arma::cx_mat operator ^(const arma::cx_mat &M1, const arma::cx_mat &M2)
{
    return kron(M1, M2);
}

arma::uword dimm(const arma::cx_mat& M)
{
    return sqrt(M.size());
}

int sgn(int value)
{
    if(value<0){ return -1; }
    return 1;
}

#endif // BASICPHISICS
