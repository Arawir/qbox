#ifndef OPERREP
#define OPERREP

#include "linearalgebra.h"
#include "iqbox.h"



enum class CalculationState
{
    StepI, Calculate, StepII
};

enum class OperMode
{
    IncreaseLeft, IncreaseRight, NotSelected
};



class OperRep
{
public:
    OperRep(iOperator* oper) :
        Operator{oper}
      , operMode{ OperMode::NotSelected }
    {

    }

    ~OperRep()
    {
        //delete Operator;
    }

    void setMode(IncreasingMode mode)
    {
        increasingMode = mode;

    }

    void prepareToSweep()
    {

    }

    void updateOperatorMode()
    {
        switch(increasingMode) {
        case IncreasingMode::RL :
            if(Operator->block() == Block::A){
                operMode = OperMode::IncreaseRight;
            } else {
                operMode = OperMode::IncreaseLeft;
            }
            break;
        case IncreasingMode::RR :
            if(Operator->block() == Block::A){
                operMode = OperMode::IncreaseRight;
            } else {
                operMode = OperMode::IncreaseRight;
            }
            break;
        }
    }

    void stepI()
    {
        State = CalculationState::StepI;
        MatrixI = Operator->mat();
    }

    void updateIfNeeded()
    {
        assert(State != CalculationState::Calculate); //dodać opis asercji

        if(State==CalculationState::StepI){
            State = CalculationState::Calculate;
            updateOperatorMode();
            increaseOperator();
            State = CalculationState::StepII;
        }
    }

    void sweepABStep()
    {
        assert(State != CalculationState::Calculate);

        if(State==CalculationState::StepI){
            State = CalculationState::Calculate;
            updateOperatorMode();
            if(Operator->block()==Block::B){
                decreaseOperator();
            } else {
                increaseOperator();
            }

            State = CalculationState::StepII;
        }
    }

    void sweepBAStep()
    {
        assert(State != CalculationState::Calculate);

        if(State==CalculationState::StepI){
            State = CalculationState::Calculate;
            updateOperatorMode();
            if(Operator->block()==Block::A){
                decreaseOperator();
            } else {
                increaseOperator();
            }

            State = CalculationState::StepII;
        }
    }

    arma::cx_mat matrixI()
    {
        return MatrixI;
    }

    arma::cx_mat matrixII()
    {
        if(State != CalculationState::StepII){
            updateIfNeeded();
        }
        return MatrixII;
    }

private:
    void increaseOperator()
    {
        switch (operMode) {
        case OperMode::IncreaseRight:
            Operator->updateStorage();
            Operator->increaseRight();
            Operator->recalculate();
            MatrixII = Operator->mat();
            break;
        case OperMode::IncreaseLeft:
            Operator->updateStorage();
            Operator->increaseLeft();
            Operator->recalculate();
            MatrixII = Operator->mat();
            break;
        }
    }

    void decreaseOperator()
    {
        MatrixI - Operator->mat();
        Operator->decrease();
        MatrixII = Operator->mat();
       // State = CalculationState::StepII; //???
    }



public: //TODO should be privzte
    iOperator* Operator; //chyba tez private
    CalculationState State;
    OperMode operMode;
    IncreasingMode increasingMode;

private:
    arma::cx_mat MatrixI;
    arma::cx_mat MatrixII;
};

#endif // OPERREP

