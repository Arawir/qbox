#ifndef LANCZOS
#define LANCZOS

#include <armadillo>


arma::cx_mat generateMatrixV(arma::cx_vec v[],
                             arma::uword m)
{
    arma::cx_mat V {v[0].size(), m, arma::fill::zeros};
    for(arma::uword i=0; i<m; i++){
        V.col(i) = v[i];
    }
    return V;
}

arma::cx_mat generateMatrixT(double alpha[],
                             double betha[],
                             arma::uword m)
{
    arma::cx_mat T{m, m, arma::fill::zeros};
    T(0,0) = alpha[0];
    for(uint i=0; i<m-1; i++){
        T(i,i+1) = betha[i+1];
        T(i+1,i) = betha[i+1];
        T(i+1,i+1) = alpha[i+1];
    }
    return T;
}

std::pair<arma::cx_mat, arma::cx_mat> lanczosDecomposition(arma::cx_mat Matrix,
                                                           arma::uword m)
{
    arma::uword Dim = sqrt( Matrix.size() );

    arma::cx_vec v[m];
    arma::cx_vec wp[m];
    arma::cx_vec w[m];
    double alpha[m];
    double betha[m];

    v[0] = normalise(arma::cx_vec{Dim, arma::fill::ones});
    wp[0] = Matrix*v[0];
    alpha[0] = real(as_scalar( trans(conj(wp[0]))*v[0]));
    w[0] = wp[0] - alpha[0]*v[0];

    for(arma::uword j=1; j<m; j++){
        betha[j] = norm( w[j-1] );
        if(betha[j]!=0){ v[j] = w[j-1]/betha[j]; }
        else{ } // dodac wektor ortogonalny
        wp[j] = Matrix*v[j];
        alpha[j] = real(as_scalar( trans(conj(wp[j]))*v[j]));
        w[j] = wp[j] - alpha[j]*v[j]-betha[j]*v[j-1];
    }

    arma::cx_mat T = generateMatrixT(alpha, betha, m);
    arma::cx_mat V = generateMatrixV(v,m);

    return std::make_pair(V, T);
}

std::pair<double, arma::cx_mat> calculateBaseStateLanczos(arma::cx_mat Matrix)
{
    auto Data = lanczosDecomposition(Matrix, std::min( static_cast<int>(rank(Matrix)), 100) );
    arma::cx_mat V = std::get<0>(Data);
    arma::cx_mat T = std::get<1>(Data);

    arma::vec Eigenvalues;
    arma::cx_mat Eigenvectors;
    eig_sym(Eigenvalues, Eigenvectors, T);

    return std::make_pair(Eigenvalues[0], V*Eigenvectors.col(0));
}
#endif // LANCZOS
