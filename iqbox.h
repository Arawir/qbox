#ifndef IQBOX_H
#define IQBOX_H

#include "ioperator.h"
#include "iparameter.h"

enum class IncreasingMode
{
    RL, RR
};

class iQBox
{
public:
    iQBox(){ }
    virtual ~iQBox() = default;

    virtual void setNodeRank(uint nodeRank) = 0;
    virtual void setBlockRank(uint blockRank) = 0;
    virtual void setIncreasingMode(IncreasingMode mode) = 0;
    virtual void setSuperblockHamiltonian(iOperator* hamiltonian) = 0;
    virtual void setHamiltonianA(iOperator* hamiltonian) = 0;
    virtual void setHamiltonianB(iOperator* hamiltonian) = 0;
    virtual void addObservable(iOperator* observable) = 0;
    virtual void addParameter(iParameter* parameter) = 0;
    virtual void addSelector(std::function<bool(iQBox*, uint)> newSelector) = 0;
    virtual void deleteSelectors() = 0;
    virtual void addInternsweepAction(std::function<void(iQBox*)> newInternSweepAction) = 0;
    virtual void deleteInternSweepActions() = 0;

    virtual void addNodeWithTruncation() = 0;
    virtual void addNodesWithTruncation(uint numberOfNodes) = 0;

    virtual void addNode() = 0;
    virtual void addNodes(uint numberOfNodes) = 0;

    virtual void claclualteDensityMatricesLanczos() = 0;

    virtual void sweep() = 0;
    virtual void sweeps(uint numberOfSweeps) = 0;
    virtual void sweepsToDeltaE(uint maxNumberOfSweeps, double deltaE) = 0;

    virtual iOperator* observable(std::string name) = 0;
    virtual iParameter* parameter(std::string name) = 0;
    virtual arma::cx_mat densityMatrix(Block block, uint stateNumber) = 0;
    virtual uint nodeRank() = 0;
    virtual uint blockRank() = 0;
    virtual uint currentBlockRank() = 0; //TODO chyba mozna usunać
    virtual IncreasingMode mode() = 0;

    virtual arma::cx_mat oI(std::string name) = 0;
    virtual arma::cx_mat oII(std::string name) = 0;
    virtual complex pI(std::string name) = 0;
    virtual complex pII(std::string name) = 0;

    virtual double expectedValue(std::string observableName, uint stateNumber) = 0;
    virtual void newCycle() = 0;

    virtual uint numberOfNodes() = 0;
    virtual int currentlyAddedNodeNumber(Block block) = 0;
};

#endif // IQBOX_H
