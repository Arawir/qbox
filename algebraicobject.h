#ifndef ALGEBRAICOBJECT
#define ALGEBRAICOBJECT

#include "operator.h"
#include "parameter.h"


complex operator +(const Parameter& first, const complex second)
{
    return first.value() + second;
}

complex operator -(const Parameter& first, const complex second)
{
    return first.value() - second;
}

complex operator *(const Parameter& first, const complex second)
{
    return first.value() * second;
}

complex operator /(const Parameter& first, const complex second)
{
    return first.value() / second;
}
////////////////////////////////////////////////////////////////

complex operator +(const complex& first, const Parameter second)
{
    return first + second.value();
}

complex operator -(const complex& first, const Parameter second)
{
    return first - second.value();
}

complex operator *(const complex& first, const Parameter second)
{
    return first * second.value();
}

complex operator /(const complex& first, const Parameter second)
{
    return first / second.value();
}
////////////////////////////////////////////////////////////////

complex operator +(const Parameter& first, const Parameter& second)
{
    return first.value() + second.value();
}

complex operator -(const Parameter& first, const Parameter& second)
{
    return first.value() - second.value();
}

complex operator *(const Parameter& first, const Parameter& second)
{
    return first.value() * second.value();
}

complex operator /(const Parameter& first, const Parameter& second)
{
    return first.value() / second.value();
}
////////////////////////////////////////////////////////////////

arma::cx_mat operator +(const Operator& first, const arma::cx_mat& second)
{
    return first.mat() + second;
}

arma::cx_mat operator -(const Operator& first, const arma::cx_mat& second)
{
    return first.mat() - second;
}

arma::cx_mat operator *(const Operator& first, const arma::cx_mat& second)
{
    return first.mat() * second;
}

arma::cx_mat operator ^(const Operator& first, const arma::cx_mat& second)
{
    return kron( first.mat(), second);
}
////////////////////////////////////////////////////////////////

arma::cx_mat operator +(const arma::cx_mat& first, const Operator& second)
{
    return first + second.mat();
}

arma::cx_mat operator -(const arma::cx_mat& first, const Operator& second)
{
    return first - second.mat();
}

arma::cx_mat operator *(const arma::cx_mat& first, const Operator& second)
{
    return first * second.mat();
}

arma::cx_mat operator ^(const arma::cx_mat& first, const Operator& second)
{
    return kron( first, second.mat() );
}
////////////////////////////////////////////////////////////////

arma::cx_mat operator +(const Operator& first, const Operator& second)
{
    return first.mat() + second.mat();
}

arma::cx_mat operator -(const Operator& first, const Operator& second)
{
    return first.mat() - second.mat();
}

arma::cx_mat operator *(const Operator& first, const Operator& second)
{
    return first.mat() * second.mat();
}

arma::cx_mat operator ^(const Operator& first, const Operator& second)
{
    return kron( first.mat(), second.mat());
}
////////////////////////////////////////////////////////////////

arma::cx_mat operator *(const Parameter& first, const Operator& second)
{
    return first.value() * second.mat();
}

arma::cx_mat operator *(const Operator& first, const Parameter& second)
{
    return first.mat() * second.value();
}

arma::cx_mat operator /(const Operator& first, const Parameter& second)
{
    return first.mat() * (1.0/second.value());
}
////////////////////////////////////////////////////////////////

arma::cx_mat operator *(const complex& first, const Operator& second)
{
    return first * second.mat();
}

arma::cx_mat operator *(const Operator& first, const complex& second)
{
    return first.mat() * second;
}

arma::cx_mat operator /(const Operator& first, const complex& second)
{
    return first.mat() * (1.0/second);
}
////////////////////////////////////////////////////////////////

arma::cx_mat operator *(const arma::cx_mat& first, const Parameter& second)
{
    return first * second.value();
}

arma::cx_mat operator /(const arma::cx_mat& first, const Parameter& second)
{
    return first * (1.0/second.value());
}

#endif // ALGEBRAICOBJECT

