#ifndef QBOX_H
#define QBOX_H

#include "mathbox.h"


class QBox : public MathBox
{
public:
    QBox() :
      MathBox{ }
      , PositionFirstNodeA{0}
      , PositionLastNodeA{0}
      , PositionFirstNodeB{0}
      , PositionLastNodeB{0}
    {

    }

    ~QBox() = default;


    void addNode() override
    {
        updateObservablesMode(StateBox::mode()); //TODO mozliwe ze mozna rpzeniesc nizej i tylko przy zmianie modu
        stepIObservables();
        updateObservables();

        if(NumberBox::Mode == IncreasingMode::RL){
            PositionLastNodeA++;
            PositionFirstNodeB--;
        } else if(NumberBox::Mode == IncreasingMode::RR){
            PositionLastNodeA++;
            PositionLastNodeB++;
        }
        output << "Number of nodes: " << numberOfNodes() << std::endl;
    }

    void addNodes(uint numberOfNodes) override
    {
        for(uint i=0; i<numberOfNodes; i+=2){
            addNode();
        }
    }

    void addNodeWithTruncation() override
    {
        addNode();
        DMRGStep();
        CurrentBlockRank = std::min(CurrentBlockRank+NodeRank, BlockRank);
    }

    void addNodesWithTruncation(uint numberOfNodes) override
    {
        for(uint i=0; i<numberOfNodes; i+=2){
            addNodeWithTruncation();
        }
    }

    void claclualteDensityMatricesLanczos() override
    {
        arma::cx_vec BSs = calculateBaseState(SuperblockHamiltonian->mat());
        arma::cx_mat DMs = calculateDensityMatrix(BSs);
        StateBox::DensityMatrixA = partialTraceB(DMs, HamiltonianA->dim() );
        StateBox::DensityMatrixB = partialTraceA(DMs, HamiltonianB->dim() );
    }

    void sweep() override
    {
        int Position = 0;
        updateObservablesMode(StateBox::mode());
        prepareToSweep();

        while(sweepABIsPossible()){
            stepIObservables();
            sweepOperrepsABStep();
            DMRGStep();
            Position++;

            if(NumberBox::Mode == IncreasingMode::RL){
                PositionLastNodeA++;
                PositionFirstNodeB++;
            } else if(NumberBox::Mode == IncreasingMode::RR){
                PositionLastNodeA++;
                PositionLastNodeB--;
            }

            executeInternSweepActions();
        }
        while(sweepBAIsPossible()){
            stepIObservables();
            sweepOperrepsBAStep();
            DMRGStep();
            Position--;

            if(NumberBox::Mode == IncreasingMode::RL){
                PositionLastNodeA--;
                PositionFirstNodeB--;
            } else if(NumberBox::Mode == IncreasingMode::RR){
                PositionLastNodeA--;
                PositionLastNodeB++;
            }

            executeInternSweepActions();
        }
        while(Position!=0){
            stepIObservables();
            sweepOperrepsABStep();
            DMRGStep();
            Position++;
            if(NumberBox::Mode == IncreasingMode::RL){
                PositionLastNodeA++;
                PositionFirstNodeB++;
            } else if(NumberBox::Mode == IncreasingMode::RR){
                PositionLastNodeA++;
                PositionLastNodeB--;
            }
            executeInternSweepActions();
        }
    }

    void sweeps(uint numberOfSweeps) override
    {
        for(uint i=0; i<numberOfSweeps; i++){
            output << "Sweeps: " << i << std::endl;
            sweep();
        }
    }

    void sweepsToDeltaE(uint maxNumberOfSweeps, double deltaE) override
    {
        double energyA = expectedValue(SuperblockHamiltonian,0);
        double energyB;

        for(uint i=0; i<maxNumberOfSweeps; i++){
            output << "Sweep: " << i << "  "
                   << "Energy: " << energyA << "  "
                   << std::endl;
            sweep();
            energyB = expectedValue(SuperblockHamiltonian,0);
            if(sqrt( (energyA-energyB)*(energyA-energyB) )<deltaE){
                output << "Sweep: " << i+1 << "  "
                       << "Energy: " << energyB << "  "
                       << std::endl;
                break;
            }
            energyA = energyB;
        }
    }

    void newCycle() override
    {
        PositionFirstNodeA = 1;
        PositionLastNodeA = -1;
        PositionFirstNodeB = 1;
        PositionLastNodeB = -1;

        CurrentBlockRank = NodeRank;
        resetObservables();
        stepIObservables();
        StateBox::clearDensityMatrices();

        PositionFirstNodeA = 0;
        PositionLastNodeA = 0;
        PositionFirstNodeB = 0;
        PositionLastNodeB = 0;
    }

    uint numberOfNodes() override
    {
        return (PositionLastNodeA-PositionFirstNodeA) + (PositionLastNodeB-PositionFirstNodeB);
    }

    int currentlyAddedNodeNumber(Block block) override
    {
        if(NumberBox::Mode == IncreasingMode::RL){
            if(block==Block::A){ return PositionLastNodeA + 1; }
            if(block==Block::B){ return PositionFirstNodeB - 1; }
        } else if(NumberBox::Mode == IncreasingMode::RR){
            if(block==Block::A){ return PositionLastNodeA + 1; }
            if(block==Block::B){ return PositionLastNodeB + 1; }
        }
    }

    void addSelector(std::function<bool(iQBox*, uint)> newSelector) override
    {
        LambdaSelectors.push_back(newSelector);
    }

    void deleteSelectors() override
    {
        LambdaSelectors.clear();
    }

    void addInternsweepAction(std::function<void(iQBox*)> newInternSweepAction) override
    {
        InternSweepActions.push_back(newInternSweepAction);
    }

    void deleteInternSweepActions() override
    {
        InternSweepActions.clear();
    }

private:
    void DMRGStep()
    {
        arma::cx_vec BSs = calculateBaseState(SuperblockHamiltonian->mat());
        removeIncorrectStates(BSs);
        arma::cx_mat DMs = calculateDensityMatrix(BSs);
        arma::cx_mat DMa = partialTraceB(DMs, HamiltonianA->dim() );
        arma::cx_mat DMb = partialTraceA(DMs, HamiltonianB->dim() );
        std::vector<Eigen> EigensA = calculateEigens(DMa);
        std::vector<Eigen> EigensB = calculateEigens(DMb);
        sortEigensViaMagnitude(EigensA);
        sortEigensViaMagnitude(EigensB);
        std::vector<arma::cx_vec> BVa = extractEigenvectors(EigensA);
        std::vector<arma::cx_vec> BVb = extractEigenvectors(EigensB);
        std::vector<arma::cx_vec> BVab = mergeEigenvectors(BVa, BVb);

        StateBox::calculateDensityMatrices(EigensA, EigensB);
        rebaseOperators(BVa, BVb, BVab);
    }

    void executeInternSweepActions()
    {
        for(auto action : InternSweepActions){
            action(this);
        }
    }

    void removeIncorrectStates(arma::cx_vec &BSs)
    {
        for(uint i=0; i<BSs.size(); i++){
            for(auto selector : LambdaSelectors){
                if(selector(this, i)==false){
                    BSs(i) = 0.0; break;
                }
            }
        }
        BSs /= as_scalar(BSs.t()*BSs);
    }

    void rebaseOperators(const std::vector<arma::cx_vec> &BVa,
                         const std::vector<arma::cx_vec> &BVb,
                         const std::vector<arma::cx_vec> &BVab)
    {
        for(auto& observable : Repository::Observables){
            if(observable.Operator->block() == Block::A){
                observable.Operator->rebaseWithTruncation(BVa, std::min(BlockRank, (uint)BVa.size()) );
            } else if(observable.Operator->block() == Block::B ){
                observable.Operator->rebaseWithTruncation(BVb, std::min(BlockRank, (uint)BVb.size()) );
            } else if(observable.Operator->block() == Block::AB ){
                observable.Operator->rebaseWithTruncation(BVab, std::min(BlockRank*BlockRank, (uint)BVab.size()) );
            } else {
                assert("Cannot rebase operators - wrong block type" && false);
            }
        }
    }

    void updateObservablesMode(IncreasingMode mode)
    {
        for(auto& observable : Repository::Observables ){
            observable.setMode(mode);
        }
    }

    void stepIObservables()
    {
        for(auto& observable : Repository::Observables ){
            observable.stepI();
        }
    }

    void resetObservables()
    {
        for(auto& observable : Repository::Observables){
            observable.Operator->newCycle();
        }
    }

    void updateObservables()
    {
        for(auto& observable : Repository::Observables ){
            observable.updateIfNeeded();
        }
    }

    void sweepOperrepsABStep()
    {
        for(auto& observable : Repository::Observables ){
            observable.sweepABStep();
        }
    }

    void sweepOperrepsBAStep()
    {
        for(auto& observable : Repository::Observables ){
            observable.sweepBAStep();
        }
    }

    bool sweepABIsPossible()
    {
        return HamiltonianB->decreseIsEnable();
    }

    bool sweepBAIsPossible()
    {
        return HamiltonianA->decreseIsEnable();
    }

    void prepareToSweep()
    {
        for(auto& observable : Repository::Observables ){
            observable.prepareToSweep();
        }
    }

private:
    int PositionFirstNodeA;
    int PositionLastNodeA;
    int PositionFirstNodeB;
    int PositionLastNodeB;

    std::vector<std::function<bool(iQBox*, uint)>> LambdaSelectors;
    std::vector<std::function<void(iQBox*)>> InternSweepActions;

};

#endif // QBOX_H
